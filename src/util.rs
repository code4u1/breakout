use ggez::graphics::mint::Point2;

pub struct Area {
    pos: Point2<f32>,
    width: f32,
    height: f32
}

impl Area {
    pub const fn new(_pos: Point2<f32>, _width: f32, _height: f32) -> Area {
        Area {
            pos: _pos,
            width: _width,
            height: _height
        }
    }
}

pub fn check_point_position_to_area(position :Point2<f32>, area: &Area) -> bool {
    position.x >= area.pos.x && position.x <= area.pos.x + area.width
    && position.y >= area.pos.y && position.y <= area.pos.y + area.height
}