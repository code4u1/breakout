use std::fmt;
use crate::constants::{X_BRICK_SCALE, Y_BRICK_SCALE, X_BROKEN_BRICK_SCALE, Y_BROKEN_BRICK_SCALE};
use ggez::graphics::mint::Point2;
use crate::assets::Assets;
use ggez::graphics;

pub enum Axis {
    HORIZONTAL_AXIS,
    VERTICAL_AXIS
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    NEUTRAL
}

impl Direction {
    pub fn inverse(&self) -> Direction {
        match *self {
            Direction::UP => Direction::DOWN,
            Direction::DOWN => Direction::UP,
            Direction::LEFT => Direction::RIGHT,
            Direction::RIGHT => Direction::LEFT,
            _ => Direction::NEUTRAL
        }
    }

    pub fn get_coefficient(&self) -> f32 {
        match *self {
            Direction::UP => -1.0,
            Direction::DOWN => 1.0,
            Direction::LEFT => -1.0,
            Direction::RIGHT => 1.0,
            Direction::NEUTRAL => 0.0
        }
    }
}

impl fmt::Display for Direction {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Direction::UP => write!(f, "UP"),
            Direction::DOWN => write!(f, "DOWN"),
            Direction::RIGHT => write!(f, "RIGHT"),
            Direction::LEFT => write!(f, "LEFT"),
            Direction::NEUTRAL => write!(f, "NEUTRAL")
        }
    }
}

#[derive(Eq, PartialEq)]
pub enum GameState {
    MENU,
    GAMEPLAY,
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum BrickState {
    NORMAL,
    BROKEN
}

impl BrickState {
    pub fn get_scalability (&self) -> Point2<f32> {
        match self {
            BrickState::NORMAL => Point2 {
                x: X_BRICK_SCALE,
                y: Y_BRICK_SCALE
            },
            BrickState::BROKEN => Point2 {
                x: X_BROKEN_BRICK_SCALE,
                y: Y_BROKEN_BRICK_SCALE
            }
        }
    }

    pub fn get_image(&self, assets: &Assets) -> graphics::Image {
        match self {
            BrickState::NORMAL => assets.brick_image.clone(),
            BrickState::BROKEN => assets.brick_broken_image.clone()
        }
    }
}