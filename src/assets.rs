use ggez::audio;
use ggez::graphics;
use ggez::mint::Point2;
use ggez::{Context, GameResult};
use std::fmt::Debug;

pub struct Assets {
    pub paddle_image: graphics::Image,
    pub ball_image: graphics::Image,
    pub upper_line_image: graphics::Image,
    pub brick_image: graphics::Image,
    pub brick_broken_image: graphics::Image
    //pub hit_sound: audio::Source,
}

impl Assets {
    pub fn new(ctx: &mut Context) -> GameResult<Assets> {
        let paddle_image = graphics::Image::new(ctx, "/paddle.png")?;
        let ball_image = graphics::Image::new(ctx, "/ball2.png")?;
        let upper_line_image = graphics::Image::new(ctx, "/upper_line.png")?;
        let brick_image = graphics::Image::new(ctx, "/brick.png")?;
        let brick_broken_image = graphics::Image::new(ctx, "/broken-brick.png")?;

        Ok(Assets {
            paddle_image, ball_image, upper_line_image, brick_image, brick_broken_image
        })
    }
}