use ggez::graphics::mint::{Point2, Vector2};
use ggez::{GameResult, Context, graphics};
use crate::assets::Assets;
use ggez::graphics::Rect;

pub(crate) struct UpperLine {
    position: Point2<f32>,
    width: f32
}

impl UpperLine {
    pub fn new(pos: Point2<f32>, screen_width: f32) -> UpperLine {
        UpperLine {
            position: pos,
            width: screen_width
        }
    }

    pub fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        let rectangle = graphics::Mesh::new_rectangle(
            ctx,
            graphics::DrawMode::fill(),
            Rect::new(self.position.x, self.position.y, self.width, 2.0),
            [255.0, 255.0, 255.0, 255.0].into(),
        )?;
        graphics::draw(ctx, &rectangle, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;
        Ok(())
    }
}