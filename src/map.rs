use rand::Rng;

use ggez::{Context, graphics, GameResult};
use ggez::graphics::{Rect};
use ggez::graphics::mint::{Point2, Vector2};

use crate::assets::Assets;
use crate::Ball;
use crate::enums::{Axis, BrickState};
use crate::constants::{SCREEN_WIDTH, SCREEN_HEIGHT};

pub const MAP_HEIGHT: usize = 4;
pub const MAP_WIDTH: usize = 10;

pub struct Brick {
    pub position: Point2<f32>,
    pub is_destroyed: bool,
    pub hits: i16,
    pub height: f32,
    pub width: f32,
    pub state: BrickState,
    pub is_updatable: bool
}

impl Brick {
    pub const BRICK_HEIGHT: f32 = 30.0;
    pub const BRICK_WIDTH: f32 = 60.0;

    pub fn new(pos: Point2<f32>) -> Brick {

        Brick {
            position: pos,
            is_destroyed: false,
            hits: 0,
            height: Self::BRICK_HEIGHT,
            width: Self::BRICK_WIDTH,
            state: BrickState::NORMAL,
            is_updatable: true
        }
    }

    pub fn update(&mut self, ball: &mut Ball) -> bool {
        if !self.is_updatable {
            self.is_updatable = true;
            return false;
        }

        if self.is_destroyed {
            return false;
        }

        if self.check_horizontal_wall_hit(&ball) {
            ball.inverse_direction(&Axis::VERTICAL_AXIS);
            self.define_brick_state_depending_on_hits();
            return true;
        }

        if self.check_vertical_wall_hit(&ball) {
            ball.inverse_direction(&Axis::HORIZONTAL_AXIS);
            self.define_brick_state_depending_on_hits();
            return true;
        }

        return false;
    }

    pub fn draw(&self, ctx: &mut Context, assets: &Assets) -> GameResult<()> {
        if !self.is_destroyed {
            let scale_data = self.state.get_scalability();
            let image = self.state.get_image(&assets);

            let draw_params = graphics::DrawParam::default().
                dest(self.position).
                scale(Vector2 { x: scale_data.x, y: scale_data.y }).
                offset(Point2 { x: 0.0, y: 0.0 });

            let rectangle = graphics::Mesh::new_rectangle(
                ctx,
                graphics::DrawMode::fill(),
                Rect::new(self.position.x, self.position.y, self.width, self.height),
                [1.0, 0.5, 0.0, 1.0].into(),
            )?;
            graphics::draw(ctx, &image, draw_params)?;
            // graphics::draw(ctx, &rectangle, (ggez::mint::Point2 { x: 0.0, y: 0.0 }, ))?;
        }
        Ok(())
    }

    fn define_brick_state_depending_on_hits(&mut self) {
        match self.hits {
            0 => {
                self.hits += 1;
                self.state = BrickState::BROKEN;
                self.is_updatable = false;
            }
            1 => self.is_destroyed = true,
            _ => {}
        };
    }

    fn check_horizontal_wall_hit(&self, ball: &Ball) -> bool {
        ball.get_position().x + Ball::RADIUS >= self.position.x
            && ball.get_position().x - Ball::RADIUS <= self.position.x + self.width
            && ((ball.get_position().y >= self.position.y - Ball::RADIUS
            && ball.get_position().y <= self.position.y + Ball::RADIUS)
            || (ball.get_position().y >= self.position.y + self.height - Ball::RADIUS
            && ball.get_position().y <= self.position.y + self.height + Ball::RADIUS))
    }

    fn check_vertical_wall_hit(&self, ball: &Ball) -> bool {
        ball.get_position().y + Ball::RADIUS >= self.position.y
            && ball.get_position().y - Ball::RADIUS <= self.position.y + self.height
            && ((ball.get_position().x >= self.position.x - Ball::RADIUS
            && ball.get_position().x <= self.position.x + Ball::RADIUS)
            || (ball.get_position().x >= self.position.x + self.width - Ball::RADIUS
            && ball.get_position().x <= self.position.x + self.height + Ball::RADIUS))
    }
}

pub struct BrickField {
    field: Vec<Vec<Brick>>,
    destroyed_bricks: i32,
}

impl BrickField {
    pub const SCORE_DATA_POSITION: Point2<f32> = Point2 {
        x: 5.0,
        y: 5.0
    };

    pub fn new(starting_point: Point2<f32>) -> BrickField {
        let mut map: Vec<Vec<Brick>> = Vec::new();
        let mut current_point = Point2::from(starting_point);

        for row in 0..MAP_HEIGHT {
            let mut row_of_bricks: Vec<Brick> = Vec::new();
            for col in 0..MAP_WIDTH {
                let brick: Brick = Brick::new(current_point);
                row_of_bricks.push(brick);

                current_point.x += (Brick::BRICK_WIDTH + 10.0);
            }
            map.push(row_of_bricks);
            current_point.x = starting_point.x;
            current_point.y += (Brick::BRICK_HEIGHT + 10.0);
        }

        BrickField {
            field: map,
            destroyed_bricks: 0
        }
    }

    pub fn update(&mut self, ball: &mut Ball) {
        for row in 0..MAP_HEIGHT {
            for col in 0..MAP_WIDTH {
                let result = self.field[row][col].update(ball);
                if result {
                    self.destroyed_bricks += 1;
                    return;
                }
            }
        }
    }

    pub fn draw(&self, ctx: &mut Context, assets: &Assets) -> GameResult<()> {
        for row in 0..MAP_HEIGHT {
            for col in 0..MAP_WIDTH {
                self.field[row][col].draw(ctx, assets);
            }
        }

        let font = graphics::Font::new(ctx, "/FontsFree-Net-Translator.ttf")?;
        let mut text = graphics::Text::new(format!("Score {}", self.destroyed_bricks));
        text.set_font(font, graphics::PxScale::from(40.0));

        graphics::draw(ctx, &text, graphics::DrawParam::default().dest(Self::SCORE_DATA_POSITION))?;
        Ok(())
    }

    pub fn is_clear(&self) -> bool {
        self.destroyed_bricks == 2 * MAP_WIDTH as i32 * MAP_HEIGHT as i32
    }

    pub fn get_score(&self) -> i32 {
        self.destroyed_bricks
    }
}


#[test]
fn test_brick_update() {
    let mut ball: Ball = Ball::new(Point2 {
        x: SCREEN_WIDTH,
        y: 5.0,
    });

    let mut brick: Brick = Brick::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT - 10.0,
    });

    brick.is_updatable = false;

    assert_eq!(brick.update(&mut ball), false);
}

#[test]
fn test_brick_update_colliding_horizontal_wall_normal_to_broken() {
    let epsilon: f32 = Ball::RADIUS - 1.5;     // epsilon < ball.radius
    let mut ball: Ball = Ball::new(Point2 {
        x: SCREEN_WIDTH / 2.0 + Brick::BRICK_WIDTH - 10.0,
        y: SCREEN_HEIGHT / 2.0 + epsilon,
    });

    let mut brick: Brick = Brick::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT / 2.0,
    });

    let result = brick.update(&mut ball);
    assert_eq!(result, true);
    assert_eq!(brick.is_updatable, false);
    assert_eq!(brick.hits, 1);
    assert_eq!(brick.is_destroyed, false);
    assert_eq!(brick.state, BrickState::BROKEN);
}

#[test]
fn test_brick_update_colliding_horizontal_wall_broken_to_destroyed() {
    let epsilon: f32 = Ball::RADIUS - 1.5;     // epsilon < ball.radius
    let mut ball: Ball = Ball::new(Point2 {
        x: SCREEN_WIDTH / 2.0 + Brick::BRICK_WIDTH - 10.0,
        y: SCREEN_HEIGHT / 2.0 + epsilon,
    });

    let mut brick: Brick = Brick::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT / 2.0,
    });
    brick.hits = 1;
    brick.state = BrickState::BROKEN;

    let result = brick.update(&mut ball);
    assert_eq!(result, true);
    assert_eq!(brick.is_destroyed, true);
}

#[test]
fn test_brick_update_colliding_vertical_wall_positive_epsilon_right() {
    let epsilon: f32 = Ball::RADIUS - 1.5;     // epsilon < ball.radius
    let mut ball: Ball = Ball::new(Point2 {
        x: SCREEN_WIDTH / 2.0 + epsilon,
        y: SCREEN_HEIGHT / 2.0 + Brick::BRICK_HEIGHT - 5.0,
    });

    let mut brick: Brick = Brick::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT / 2.0,
    });

    let result = brick.update(&mut ball);
    assert_eq!(result, true);
    assert_eq!(brick.is_updatable, false);
    assert_eq!(brick.hits, 1);
    assert_eq!(brick.is_destroyed, false);
    assert_eq!(brick.state, BrickState::BROKEN);
}

#[test]
fn test_brick_update_colliding_vertical_wall_negative_epsilon_right() {
    let epsilon: f32 = Ball::RADIUS - 1.5;     // epsilon < ball.radius
    let mut ball: Ball = Ball::new(Point2 {
        x: SCREEN_WIDTH / 2.0 - epsilon,
        y: SCREEN_HEIGHT / 2.0 + Brick::BRICK_HEIGHT - 5.0,
    });

    let mut brick: Brick = Brick::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT / 2.0,
    });

    let result = brick.update(&mut ball);
    assert_eq!(result, true);
    assert_eq!(brick.is_updatable, false);
    assert_eq!(brick.hits, 1);
    assert_eq!(brick.is_destroyed, false);
    assert_eq!(brick.state, BrickState::BROKEN);
}

#[test]
fn test_brick_update_colliding_vertical_wall_left() {
    let epsilon: f32 = Ball::RADIUS - 1.5;     // epsilon < ball.radius
    let mut ball: Ball = Ball::new(Point2 {
        x: SCREEN_WIDTH / 2.0 - epsilon,
        y: SCREEN_HEIGHT / 2.0 + 5.0,
    });

    let mut brick: Brick = Brick::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT / 2.0,
    });

    let result = brick.update(&mut ball);
    assert_eq!(result, true);
    assert_eq!(brick.is_updatable, false);
    assert_eq!(brick.hits, 1);
    assert_eq!(brick.is_destroyed, false);
    assert_eq!(brick.state, BrickState::BROKEN);
}

#[test]
fn test_brick_update_colliding_horizontal_wall_left() {
    let epsilon: f32 = Ball::RADIUS - 1.5;     // epsilon < ball.radius
    let mut ball: Ball = Ball::new(Point2 {
        x: SCREEN_WIDTH / 2.0 + 10.0,
        y: SCREEN_HEIGHT / 2.0 + epsilon,
    });

    let mut brick: Brick = Brick::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT / 2.0,
    });

    let result = brick.update(&mut ball);
    assert_eq!(result, true);
    assert_eq!(brick.is_updatable, false);
    assert_eq!(brick.hits, 1);
    assert_eq!(brick.is_destroyed, false);
    assert_eq!(brick.state, BrickState::BROKEN);
}