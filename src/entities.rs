use ggez::{Context, GameResult, graphics};
use crate::assets::Assets;
use std::fmt;
use ggez::graphics::mint::{Point2, Vector2};
use crate::constants::{SCREEN_WIDTH, SCREEN_HEIGHT, Y_UPPER_LIMIT};
use crate::enums::{Axis, Direction};

pub struct Ball {
    position: Point2<f32>,
    radius: f32,
    speed: f32,
    vertical_direction: Direction,
    horizontal_direction: Direction,
    deviation: f32,
    lost: bool,
}

impl Ball {
    pub const RADIUS: f32 = 5.0;
    pub const SPEED: f32 = 250.0;
    pub fn new(pos: Point2<f32>) -> Self {
        Ball {
            position: pos,
            radius: Self::RADIUS,
            speed: Self::SPEED,
            vertical_direction: Direction::UP,
            horizontal_direction: Direction::NEUTRAL,
            deviation: 0.0,
            lost: false,
        }
    }

    pub fn define_ball_directions(&mut self, paddle: &Paddle) {
        if self.position.x >= SCREEN_WIDTH - 2.5 || self.position.x <= 2.5 {
            self.inverse_direction(&Axis::HORIZONTAL_AXIS);
        }

        if self.position.y <= Y_UPPER_LIMIT {
            self.inverse_direction(&Axis::VERTICAL_AXIS);
        }

        if self.position.y >= paddle.position.y - self.radius
            && self.position.y <= paddle.position.y
            && self.position.x >= paddle.position.x
            && self.position.x <= paddle.position.x + paddle.width as f32 {
            if self.position.x > paddle.position.x + paddle.width as f32 / 2.0 {
                self.horizontal_direction = Direction::RIGHT;
            } else {
                self.horizontal_direction = Direction::LEFT;
            }

            let paddle_center = paddle.position.x + (paddle.width as f32 / 2.0);
            let center_distant = (paddle_center - self.position.x).abs();

            self.deviation = center_distant / ((paddle.width as f32) / 2.0);
            self.inverse_direction(&Axis::VERTICAL_AXIS);
        }

        if self.position.y >= SCREEN_HEIGHT {
            self.lost = true;
            return;
        }
    }

    pub fn update(&mut self, seconds: f32, paddle: &Paddle) {
        let vert_coefficient = self.vertical_direction.get_coefficient();
        let hor_coefficient = self.horizontal_direction.get_coefficient();

        let new_pos_y = self.position.y + Self::SPEED * seconds * vert_coefficient;
        let new_pos_x = self.position.x + Self::SPEED * seconds * self.deviation * hor_coefficient;

        self.position.y = nalgebra::clamp(new_pos_y, 2.5, SCREEN_HEIGHT);
        self.position.x = nalgebra::clamp(new_pos_x, 2.5, SCREEN_WIDTH - 2.5);

        self.define_ball_directions(paddle);
    }

    pub fn draw(&self, ctx: &mut Context, assets: &Assets) -> GameResult<()> {
        let draw_params = graphics::DrawParam::default().
            dest(self.position).
            scale(Vector2 { x: 0.25, y: 0.25 }).
            offset(Point2 { x: 0.32, y: 0.27 });
        // let circle = graphics::Mesh::new_circle(
        //     ctx,
        //     graphics::DrawMode::fill(),
        //     Vec2::new(self.position.x.into(), self.position.y.into()),
        //     self.radius,
        //     1.0,
        //     Color::new(1.0, 0.0, 1.0, 1.0)
        // )?;
        graphics::draw(ctx, &assets.ball_image, draw_params)?;
        // graphics::draw(ctx, &circle, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;
        Ok(())
    }

    pub fn is_lost(&self) -> bool {
        self.lost
    }

    pub fn get_position(&self) -> Point2<f32> {
        self.position
    }

    pub fn inverse_direction(&mut self, axis: &Axis) {
        match axis {
            Axis::HORIZONTAL_AXIS => {
                self.horizontal_direction = self.horizontal_direction.inverse();
            }
            Axis::VERTICAL_AXIS => {
                self.vertical_direction = self.vertical_direction.inverse();
            }
        }
    }

    pub fn set_horizontal_direction(&mut self, direction: Direction) {
        self.horizontal_direction = direction;
    }

    pub fn get_horizontal_direction(&self) -> Direction {
        self.horizontal_direction
    }

    pub fn get_vertical_direction(&self) -> Direction {
        self.vertical_direction
    }

    pub fn set_vertical_direction(&mut self, direction: Direction) {
        self.vertical_direction = direction;
    }
}

// -----------------------------------------------------------------------------------------

pub struct Paddle {
    position: Point2<f32>,
    width: i16,
    height: i16,
    is_active: bool,
}

impl Paddle {
    pub const SPEED: f32 = 500.0;
    pub const PADDLE_WIDTH: i16 = 80;
    pub const PADDLE_HEIGHT: i16 = 15;

    pub fn new(pos: Point2<f32>) -> Self {
        Paddle {
            position: pos,
            width: Self::PADDLE_WIDTH,
            height: Self::PADDLE_HEIGHT,
            is_active: false,
        }
    }

    pub fn update(&mut self, amount: f32, seconds: f32) {
        let new_pos = self.position.x + Self::SPEED * seconds * amount;
        self.position.x = nalgebra::clamp(new_pos, 0.0, SCREEN_WIDTH);
    }

    pub fn draw(&self, ctx: &mut Context, assets: &Assets) -> GameResult<()> {
        let draw_params = graphics::DrawParam::default().
            dest(self.position).
            scale(Vector2 { x: 0.35, y: 0.4 }).
            offset(Point2 { x: 0.0, y: 0.2 });
        // let rectangle = graphics::Mesh::new_rectangle(
        //     ctx,
        //     graphics::DrawMode::fill(),
        //     Rect::new(self.position.x, self.position.y, self.width as f32, self.height as f32),
        //     [1.0, 0.5, 0.0, 1.0].into(),
        // )?;
        graphics::draw(ctx, &assets.paddle_image, draw_params)?;
        // graphics::draw(ctx, &rectangle, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;
        Ok(())
    }
}


// UNIT TESTS


#[test]
fn test_defining_ball_directions_inverse_horizontal() {
    let mut ball: Ball = Ball::new(Point2 {
        x: SCREEN_WIDTH,
        y: 5.0,
    });

    let paddle: Paddle = Paddle::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT - 10.0,
    });

    ball.set_horizontal_direction(Direction::RIGHT);

    ball.define_ball_directions(&paddle);
    assert_eq!(ball.get_horizontal_direction(), Direction::LEFT);
}

#[test]
fn test_defining_ball_directions_inverse_vertical() {
    let mut ball: Ball = Ball::new(Point2 {
        x: 50.0,
        y: Y_UPPER_LIMIT,
    });

    let paddle: Paddle = Paddle::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT - 10.0,
    });

    ball.define_ball_directions(&paddle);
    assert_eq!(ball.get_vertical_direction(), Direction::DOWN);
}

#[test]
fn test_defining_ball_directions_hit_paddle_right() {
    let mut ball: Ball = Ball::new(Point2 {
        x: SCREEN_WIDTH / 2.0 + Paddle::PADDLE_WIDTH as f32 - 10.0,
        y: SCREEN_HEIGHT - 12.0,
    });

    let paddle: Paddle = Paddle::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT - 10.0,
    });

    ball.set_vertical_direction(Direction::DOWN);

    ball.define_ball_directions(&paddle);
    assert_eq!(ball.get_horizontal_direction(), Direction::RIGHT);
    assert_eq!(ball.get_vertical_direction(), Direction::UP);
}

#[test]
fn test_defining_ball_directions_hit_paddle_left() {
    let mut ball: Ball = Ball::new(Point2 {
        x: SCREEN_WIDTH / 2.0 + 10.0,
        y: SCREEN_HEIGHT - 12.0,
    });

    let paddle: Paddle = Paddle::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT - 10.0,
    });

    ball.set_vertical_direction(Direction::DOWN);

    ball.define_ball_directions(&paddle);
    assert_eq!(ball.get_horizontal_direction(), Direction::LEFT);
    assert_eq!(ball.get_vertical_direction(), Direction::UP);
}

#[test]
fn test_defining_ball_directions_lost_ball() {
    let mut ball: Ball = Ball::new(Point2 {
        x: SCREEN_WIDTH / 4.0,
        y: SCREEN_HEIGHT,
    });

    let paddle: Paddle = Paddle::new(Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT - 10.0,
    });

    ball.set_vertical_direction(Direction::DOWN);

    ball.define_ball_directions(&paddle);
    assert_eq!(ball.is_lost(), true);
}



