mod assets;
mod map;
mod game_play_info;
mod entities;
mod constants;
mod enums;
mod util;

use ggez::event::{KeyCode, KeyMods, MouseButton};
use ggez::{graphics, Context, GameResult, event, timer, input, ContextBuilder, filesystem};
use ggez::graphics::{Rect, Color};
use ggez::winit::event::VirtualKeyCode;

use glam::*;
use ggez::graphics::mint::{Point2, Vector2};
use ggez::conf::{Conf, WindowMode};

use std::{env, fmt};
use std::path;

use crate::assets::{Assets};
use crate::map::{Brick, BrickField};
use crate::game_play_info::{UpperLine};
use crate::entities::{Paddle, Ball};
use crate::constants::{SCREEN_WIDTH, SCREEN_HEIGHT, MENU_LABELS, LABEL_DISTANCE, PLAY_LABEL_AREA};
use crate::enums::GameState;
use crate::util::check_point_position_to_area;

#[derive(Debug, Default)]
struct InputState {
    movement: f32
}

struct Game {
    screen_width: f32,
    screen_height: f32,
    assets: Assets,
    input: InputState,
    player: Paddle,
    ball: Ball,
    brick_field: BrickField,
    upper_line: UpperLine,
    game_over: bool,
    victory: bool,
    game_state: GameState
}

impl Game {
    /// Our new function will set up the initial state of our game.
    fn new(ctx: &mut Context, conf: &Conf) -> GameResult<Game> {
        let screen_width = conf.window_mode.width;
        let screen_height = conf.window_mode.height;

        let assets = Assets::new(ctx)?;

        let player_pos = Point2 {
            x: screen_width / 2.0,
            y: screen_height - 10.0
        };
        let ball_pos = Point2 {
            x: screen_width / 2.0,
            y: screen_height - 12.5
        };

        let brick_field_pos = Point2 {
            x: 50.0,
            y: 100.0
        };

        let upper_line_position = Point2 {
            x: 0.0,
            y: 46.0
        };

        let obj = Game {
            screen_width: conf.window_mode.width,
            screen_height: conf.window_mode.height,
            assets: assets,
            input: InputState::default(),
            player: Paddle::new(player_pos),
            ball: Ball::new(ball_pos),
            brick_field: BrickField::new(brick_field_pos),
            upper_line: UpperLine::new(upper_line_position, SCREEN_WIDTH),
            game_over: false,
            victory: false,
            game_state: GameState::MENU
        };

        Ok(obj)
    }

    pub fn draw_gameplay(&mut self, ctx: &mut Context) -> GameResult {
        let dark_blue = graphics::Color::from_rgb(26, 51, 77);
        graphics::clear(ctx, dark_blue);

        if self.ball.is_lost() {
            let font = graphics::Font::new(ctx, "/FontsFree-Net-Translator.ttf")?;
            let mut text = graphics::Text::new(format!("Game Over\nYour Score is {}", self.brick_field.get_score()));
            text.set_font(font, graphics::PxScale::from(40.0));

            let top_left = Point2 {
                x: (self.screen_width - text.width(ctx)) / 2.0,
                y: (self.screen_height - text.height(ctx)) / 2.0,
            };
            graphics::draw(ctx, &text, graphics::DrawParam::default().dest(top_left))?;
            graphics::present(ctx)?;
            return Ok(())
        }

        if self.brick_field.is_clear() {
            let font = graphics::Font::new(ctx, "/FontsFree-Net-Translator.ttf")?;
            let mut text = graphics::Text::new(format!("Congratulations\nYour Score {}", self.brick_field.get_score()));
            text.set_font(font, graphics::PxScale::from(40.0));

            let top_left = Point2 {
                x: (self.screen_width - text.width(ctx)) / 2.0,
                y: (self.screen_height - text.height(ctx)) / 2.0,
            };
            graphics::draw(ctx, &text, graphics::DrawParam::default().dest(top_left))?;
            graphics::present(ctx)?;
            return Ok(())
        }

        self.player.draw(ctx, &self.assets)?;
        self.ball.draw(ctx, &self.assets)?;
        self.brick_field.draw(ctx, &self.assets);
        self.upper_line.draw(ctx);

        graphics::present(ctx)?;
        Ok(())
    }

    pub fn draw_menu(&mut self, ctx: &mut Context) -> GameResult {
        let dark_blue = graphics::Color::from_rgb(26, 51, 77);
        graphics::clear(ctx, dark_blue);

        let font = graphics::Font::new(ctx, "/FontsFree-Net-Translator.ttf")?;

        for index in 0..3 {
            let mut text = graphics::Text::new(format!("{}", MENU_LABELS[index]));
            text.set_font(font, graphics::PxScale::from(40.0));

            let label_position = Point2 {
                x: (self.screen_width - text.width(ctx)) / 2.0,
                y: (self.screen_height - text.height(ctx)) / 4.0 + index as f32 * LABEL_DISTANCE,
            };
            graphics::draw(ctx, &text, graphics::DrawParam::default().dest(label_position))?;
        }
        graphics::present(ctx)?;
        Ok(())
    }
}

impl event::EventHandler<ggez::GameError> for Game {

    fn update(&mut self, ctx: &mut Context) -> GameResult {
        if self.ball.is_lost() || self.brick_field.is_clear() {
            return Ok(());
        }

        const DESIRED_FPS: u32 = 60;
        while timer::check_update_time(ctx, DESIRED_FPS) {
            let seconds = 1.0 / (DESIRED_FPS as f32);

            // Update player state
            if self.game_state.eq(&GameState::GAMEPLAY) {
                self.player.update(self.input.movement, seconds);
                self.ball.update(seconds, &self.player);
                self.brick_field.update(&mut self.ball);
            }
        }

        Ok(())
    }

    /// draw is where we should actually render the game's current state.
    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        match self.game_state {
            GameState::MENU => self.draw_menu(ctx),
            GameState::GAMEPLAY => self.draw_gameplay(ctx)
        };
        Ok(())
    }

    fn mouse_button_down_event(&mut self, _ctx: &mut Context, _button: MouseButton, _x: f32, _y: f32) {
        match _button {
            MouseButton::Left => {
                if check_point_position_to_area(Point2 { x: _x, y: _y }, &PLAY_LABEL_AREA) {
                    self.game_state = GameState::GAMEPLAY;
                }
            }
            _ => ()
        }
    }

    fn key_down_event(&mut self,
                      ctx: &mut Context,
                      keycode: event::KeyCode,
                      _keymod: input::keyboard::KeyMods,
                      _repeat: bool) {
        match keycode {
            event::KeyCode::Left => self.input.movement = -1.0,
            event::KeyCode::Right => self.input.movement = 1.0,
            _ => (), // Do nothing
        }
    }

    fn key_up_event(&mut self,
                    _ctx: &mut Context,
                    keycode: event::KeyCode,
                    _keymod: input::keyboard::KeyMods) {
        match keycode {
            event::KeyCode::Left | event::KeyCode::Right => self.input.movement = 0.0,
            _ => (), // Do nothing
        }
    }
}

// -----------------------------------------------------------------------------------------

fn main() -> GameResult {
    let conf = Conf::new().
        window_mode(WindowMode {
            width: SCREEN_WIDTH,
            height: SCREEN_HEIGHT,
            ..Default::default()
        });
    let (mut ctx, event_loop) = ContextBuilder::new("breakout", "KriKri").
        default_conf(conf.clone()).
        build().
        unwrap();

    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        filesystem::mount(&mut ctx, &path, true);
    }

    let state = Game::new(&mut ctx, &conf).unwrap();

    event::run(ctx, event_loop, state)
}
