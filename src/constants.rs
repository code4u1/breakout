use crate::util::Area;
use ggez::mint::Point2;
use crate::enums::BrickState;

pub const SCREEN_HEIGHT: f32 = 400.0;
pub const SCREEN_WIDTH: f32 = 800.0;

pub const MENU_LABELS: [&str ; 3] = ["PLAY", "OPTIONS", "INSTR"];
pub const LABEL_DISTANCE: f32 = 100.0;
pub const PLAY_LABEL_AREA: Area = Area::new(
    Point2 {
        x: SCREEN_WIDTH / 2.0 - 50.0,
        y: SCREEN_HEIGHT / 4.0
    },
    100.0,
    30.0
);

pub const OPTIONS_LABEL_AREA: Area = Area::new(
    Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT / 4.0 + LABEL_DISTANCE
    },
    70.0,
    30.0
);

pub const INSTRUCTIONS_LABEL_AREA: Area = Area::new(
    Point2 {
        x: SCREEN_WIDTH / 2.0,
        y: SCREEN_HEIGHT / 4.0 + 2.0 * LABEL_DISTANCE
    },
    70.0,
    30.0
);

pub const X_BRICK_SCALE: f32 = 0.075;
pub const Y_BRICK_SCALE: f32 = 0.075;

pub const X_BROKEN_BRICK_SCALE: f32 = 0.075;
pub const Y_BROKEN_BRICK_SCALE: f32 = 0.09;

pub const Y_UPPER_LIMIT: f32 = 50.0;